@extends("website.layouts.app")
@section('content')
    @if(Helpers::ReturnAds(1)->status ==1)

        @if(Helpers::ReturnAds(1)->type ==1)
            <section>
                <div class="container">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">

                                <div class="item active">
                                    <img src="{{Helpers::ReturnAds(1)->path}}" alt="Los Angeles" style="width:100%; height: 250px">
                                </div>

                        </div>
                    </div>
                </div>
            </section>
            @else
            <div>
                <div class="container" style="text-align: center">

                {!! Helpers::ReturnAds(1)->code !!}
                </div>
            </div>
        @endif
    @endif
    <div class="container hide-in-sm">
        <section class="banner" id="top">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 pull-left">
                        <div class="banner-caption">
                            <div class="line-dec"></div>
                            <span style="margin:0 149px">{{trans('site.nav_title')}}</span>
                            <!-- <div class="add-adv">
                                <a href="#">اضف اعلانك الان</a>
                            </div> -->
                        </div>
                        <div class="submit-form">
                            <div class="tab-content" id="pills-tabContent">

                                <div class="tab-pane fade in active" id="pills1" role="tabpanel"
                                     aria-labelledby="pills-contact-tab">
                                    <form action="/search" method="post">
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-9 first-item" style="border-right:none">
                                                <fieldset>
                                                    <input name="search" type="text" class="form-control" id="name"
                                                           placeholder="{{trans('site.search')}}">
                                                </fieldset>
                                            </div>

                                            <div class="col-md-3">
                                                <fieldset>
                                                    <button type="submit" id="form-submit" class="btn">{{trans('site.search')}}</button>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <section class="our-services" id="services">
        <div class="container">
            <div class="row ">
                <div class="col-md-2">
                    @if(Helpers::ReturnAds(4)->status ==1)

                        @if(Helpers::ReturnAds(4)->type ==1)
                            <section>
                                <div class="">
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">

                                            <div class="item active">
                                                <img src="{{Helpers::ReturnAds(4)->path}}" alt="Los Angeles" style=" height: 250px">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        @else
                            <div>
                                <div class="container" style="text-align: center">

                                    {!! Helpers::ReturnAds(4)->code !!}
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="section-heading hide-in-sm">
                                <h2>{{trans('site.categories')}}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        @foreach($categories as $category)
                            <div class="col-md-3 col-xs-4">
                                <div class="service-item">
                                    <h4>
                                        <img src="{{$category->icon}}" alt="" width="16" height="16">
                                        <a href="/categories/{{$category->name_ar}}">{{$current_lang == 'ar' ?$category->name_ar: $category->name_en}}</a>
                                    </h4>

                                    <ul class="hide-in-sm">
                                        @foreach($category->children->take(5) as $child)
                                            <li><a href="/products/{{$child->id}}"><i
                                                            class="fa fa-angle-left"></i>{{$current_lang == 'ar' ?$child->name_ar: $child->name_en}}
                                                </a></li>
                                        @endforeach
                                    </ul>

                                    <div class="more hide-in-sm">
                                        <a href="/categories/{{$category->id}}">{{trans('site.more')}} {{$current_lang == 'ar' ?$category->name_ar: $category->name_en}}</a>
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-2">
                    @if(Helpers::ReturnAds(5)->status ==1)

                        @if(Helpers::ReturnAds(5)->type ==1)
                            <section>
                                <div class="">
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">

                                            <div class="item active">
                                                <img src="{{Helpers::ReturnAds(5)->path}}" alt="Los Angeles" style=" height: 250px">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        @else
                            <div>
                                <div class="container" style="text-align: center">

                                    {!! Helpers::ReturnAds(5)->code !!}
                                </div>
                            </div>
                        @endif
                    @endif
                </div>

            </div>

        </div>
    </section>

    @if(Helpers::ReturnAds(2)->status ==1)
        @if(Helpers::ReturnAds(2)->type ==1)
            <section>
                <div class="container">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">

                            <div class="item active">
                                <img src="{{Helpers::ReturnAds(2)->path}}" alt="Los Angeles" style="width:100%; height: 250px">
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        @else
            <div>
                <div class="container" style="text-align: center">

                {!! Helpers::ReturnAds(2)->code !!}
                </div>
            </div>
        @endif
    @endif
    <section class="popular-places" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>{{trans('site.most_viewed_products')}}</h2>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme sliderrr sliderrr" dir="ltr">
                @foreach($most_viewed_products as $most_product)
                    <div class="item popular-item">
                        <div class="thumb">
                            <a href="/product/{{$most_product->id}}"><img
                                    src="{{asset($most_product->ProductImage[0]->image ??'')}}" alt=""></a>
                            <div class="text-content">
                                <a href="/product/{{$most_product->id}}">
                                    <h4>{{$most_product->title}}</h4>
                                </a>
                                @if($most_product->price)
                                    <span>{{$most_product->price}} {{ Helpers::getCurrency() }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="popular-places" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>{{trans('site.latest_products')}}</h2>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme sliderrr" dir="ltr">
                @foreach($latest_products as $latest_product)
                    <div class="item popular-item">
                        <div class="thumb">
                            <a href="/product/{{$latest_product->id}}"><img
                                    src="{{asset($latest_product->ProductImage[0]->image ?? '')}}" alt=""></a>
                            <div class="text-content">
                                <a href="/product/{{$latest_product->id}}">
                                    <h4>{{$latest_product->title}}</h4>
                                </a>
                                @if($latest_product->price)
                                    <span>{{$latest_product->price}} {{ Helpers::getCurrency() }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>





@endsection
