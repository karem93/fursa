@extends("website.layouts.app")
@section('content')
    @push('css')
        <style>
            .contact-form-card{
                background-color: #fafafa;
                border: .5px solid #dadada;
                margin-top: 20px;
                padding-top: 10px;
                padding-bottom: 40px;
            }
            .upload-file{
                min-height: 50px;
                border: .5px dashed #c53430;
                text-align: center;
                font-size: 15px;
            }
            .upload-file label{
                cursor: pointer;
                position: relative;
                top: 10px;
            }
        </style>
    @endpush

    <div class="container hide-in-sm">
        <section class="banner" id="top">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 pull-left">
                        <div class="banner-caption">
                            <div class="line-dec"></div>
                            <span style="margin:0 149px">{{trans('site.nav_title')}}</span>
                            <!-- <div class="add-adv">
                                <a href="#">اضف اعلانك الان</a>
                            </div> -->
                        </div>
                        <div class="submit-form">
                            <div class="tab-content" id="pills-tabContent">

                                <div class="tab-pane fade in active" id="pills1" role="tabpanel"
                                     aria-labelledby="pills-contact-tab">
                                    <form action="/search" method="post">
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-9 first-item" style="border-right:none">
                                                <fieldset>
                                                    <input name="search" type="text" class="form-control" id="name"
                                                           placeholder="{{trans('site.search')}}">
                                                </fieldset>
                                            </div>

                                            <div class="col-md-3">
                                                <fieldset>
                                                    <button type="submit" id="form-submit" class="btn">{{trans('site.search')}}</button>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="popular-places" id="popular">
        <div class="container">
            <div class="row col-md-12">
                <div class="col-md-2"></div>
                <div class="col-md-8 ">
                    <div class="row col-md-12 gutters-sm contact-form-card">
                        <div class="col-md-12">
                            <div class="section-heading">
                                <h2>{{trans('site.contact_us')}}</h2>
                            </div>
                        </div>
                        <div class="col-md-12 mb-12">
                            <form action="{{url('/contact_us')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleInputEmail1">{{trans('site.email')}}</label>
                                    <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                                           aria-describedby="emailHelp" placeholder="{{trans('site.email')}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">{{trans('site.category')}}</label>
                                    <select name="category_id" id="" class="form-control">
                                        <option value="">{{trans('site.choose_category')}}</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{app()->isLocale('ar') ?$category->name_ar:$category->name_en}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">{{trans('site.description')}}</label>
                                    <textarea name="description" rows="5" id="" class="form-control"></textarea>
                                    <p> {{trans('site.descr')}}</p>
                                </div>
                                <div class="form-group upload-file">
                                    <label for="exampleFileInput"> <i class="fa fa-camera"></i>  <span style="color: red"> {{trans('site.click_here')}} </span>{{trans('site.add_files')}}</label>
                                    <input style="display: none" type="file" name="image" class="form-control" id="exampleFileInput"
                                           aria-describedby="emailHelp">
                                </div>

                                <button type="submit" class="btn btn-danger btn-lg btn-block">{{trans('site.send')}}</button>
                            </form>
                        </div>

                    </div>

                </div>

            </div>




        </div>
    </section>
@endsection
