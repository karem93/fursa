<div class="overlay-citys"></div>
<style>
    .mul {
        font-size: 14px;
        width: 500px;
        padding: 12px;
        position: absolute;
        z-index: 100000;
        display: none;
        background-color: #ffffff;
        border: 3px solid #999;
        direction: rtl;
        -webkit-border-radius: 0 0 8px 8px;
        -moz-border-radius: 0 0 8px 8px;
        -o-border-radius: 0 0 8px 8px;
        border-radius: 0 0 8px 8px;
    }

    .sh, .top, .brd, .nav, .col3 h4, .phc, .sug, .ls li {
        -moz-box-shadow: 0 3px 3px #999;
        -o-box-shadow: 0 3px 3px #999;
        -webkit-box-shadow: 0 3px 3px #999;
        box-shadow: 0 3px 3px #999;
        margin-bottom: 5px;
    }

    .mul li {
        float: right;
        width: 183px;
        margin-left: 5px;
    }

    @media (min-width: 1250px) {
        .mul li {
            width: 230px;
        }
    }

    .mul ul {
        width: 183px;
        float: right;
    }

    @media (min-width: 1250px) {
        .mul ul {
            width: 230px;
        }
    }

    .ml li {
        margin-left: 5px;
        height: 40px;
        line-height: 40px;
        overflow: hidden;
        /*border-bottom: 1px solid #efefef;*/
    }

    .mul .pa {
        padding-right: 50px;
    }

    .ml a:hover {
        color: orange;
    }

    .cf.c2 {
        background-position: -32px -168px;
    }

    a {
        text-decoration: none;
    }

    .ml li a:hover {
        text-decoration: none;
    }

    .box-city a:active {
        color: orange;
    }

    .dropdown-menu-left {
        right: auto;
        left: unset;
    }
</style>
<div class="wrap">

    <div class="upper-bar">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-4 align-self-center">
                    <a href="/" class="logo-link"><img src="/website/img/rabtlogo.png" width="120px"/></a>


                </div>
                <div class="col-md-8 align-self-center hide-in-sm" @if(!auth()->check()) style="padding-top: 50px;"
                     @endif  dir="ltr">

                    <div @if(app()->isLocale('en')) style="text-align: right" @else style="text-align: left" @endif >
                        @if(auth()->check())
                            <ul class="list-unstyled d-flex align-items-center justify-content-end u-links">
                                <li class="_nav-item"><a href="/favourites"> <i class="fa fa-heart"></i>
                                        <span> {{trans('site.favs')}} </span> </a></li>
                                <li class="_nav-item"><a href="/chats"> <i class="fa fa-comments"></i>
                                        <span> {{trans('site.messages')}} </span> </a></li>
                            </ul>
                        @endif


                        <a class="clk-cit">
                            @if(session('flag'))
                                <img style="height: 15px;width: 30px;" id="header_flag_source" class="cf c9"
                                     src="{{asset(session('flag'))}}">
                            @else
                                <i class="fa fa-globe fa-2x"></i>
                            @endif
                            <i class="fa fa-angle-down "></i>
                        </a>
                        @if(auth()->check())

                            <a class="login-header btn-add-header"
                               href="{{url('add-ad?country='.session('country_id'))}}">{{trans('site.add_product')}}</a>
                        @endif
                        @if(!auth()->check())
                            <a href="/login" class="login-header"><span>{{trans('site.login')}}</span> </a>
                        @else
                            <div style="display: inline-flex;">
                            <span data-toggle="dropdown">
                                    <h4>{{auth()->user()->username}}  <i class="fa fa-caret-down"
                                                                         aria-hidden="true"></i></h4>
                            </span>
                                <ul class="dropdown-menu" style="right: unset">
                                    <li><a href="/profile" data-original-title="" title=""> {{trans('site.profile')}} <i
                                                    class="fa fa-user" aria-hidden="true"></i> </a></li>

                                    @if(auth()->user()->role == 1)
                                        <li><a href="/webadmin/dashboard" data-original-title=""
                                               title=""> {{trans('site.admin_panel')}} <i class="fa fa-users"
                                                                                          aria-hidden="true"></i> </a>
                                        </li>
                                    @endif
                                    <li><a href="/logout" data-original-title="" title=""> {{trans('site.logout')}} <i
                                                    class="fa fa-power-off" aria-hidden="true"></i> </a></li>
                                </ul>
                            </div>
                        @endif
                        <ul id="um00" class="mul sh city-select box-city"
                            @if(app()->isLocale('en')) style="right: 125.5px;left: auto;" @else style="left: 11.5px"
                            @endif style="top: 96px; display: none;">
                            <li>
                                <ul class="ml">

                                    @foreach(\App\Models\Country::get() as $country)
                                        <li onclick="UpdateCountry('{{$country->id}}','{{$country->flag}}')"
                                            class="select_country_id" country_id="{{$country->id}}"><a
                                                    href="javascript:;"><img class="cf c9"
                                                                             src="{{asset($country->flag)}}"> {{app()->isLocale('ar') ?$country->name_ar:$country->name_en}}
                                            </a></li>
                                    @endforeach

                                </ul>
                            </li>

                            <li>
                                <ul id="city_area" class="ml">
                                    @if(session('country_id'))
                                        @foreach(\App\Models\Country::find(session('country_id'))->cities as $city)
                                            <li onclick="UpdateCity('{{$city->id}}')" city_id="{{$city->id}}"
                                                class="select_city_id"><a
                                                        href="javascript:;"> {{app()->isLocale('ar') ?$city->name_ar:$city->name_en}}</a>
                                            </li>
                                        @endforeach
                                    @endif

                                </ul>
                            </li>
                        </ul>

                        <ul style="display: inline-flex">
                            <li>
                                @if(app()->isLocale('ar'))
                                    <a href="/changeLang/ar" class="dropdown-toggle" data-toggle="dropdown"> <img
                                                src="/website/img/sa.jpg" alt="" width="16" height="16"> العربية <b
                                                class="caret"></b></a>
                                @else
                                    <a href="/changeLang/en" class="dropdown-toggle" data-toggle="dropdown"> <img
                                                src="/website/img/en.png" alt="" width="16" height="16"> English <b
                                                class="caret"></b></a>
                                @endif

                                <ul class="dropdown-menu dropdown-menu-left language">
                                    <li><a href="/changeLang/en" title="English"> <img src="/website/img/en.png"
                                                                                       width="16" height="16">
                                            English</a>
                                    </li>
                                    <li><a href="/changeLang/ar" title="Arabic"><img src="/website/img/sa.jpg"
                                                                                     width="16" height="16"> العربية</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <header id="header" class="hide-in-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-category-menu col-md-12">
                        <ul class="dubizzle_menu" data-ui-id="dubizzle_menu" data-header-id="list">
                            @if(isset($categories))
                                @foreach($categories as $category)
                                    @if($category->children)
                                        <li class="dubizzle_menu_item ">
                                            <a href="/categories/{{$category->name_ar}}"
                                               class="dubizzle_menu_item_link header_link"
                                               data-tr-event-name="click_vertical_selected"
                                               data-tr-context="{'cL1Id': 7, 'cL2Id': '', 'cL3Id': '', 'cL4Id': '', 'categoryId': 7}"
                                               data-ui-id="menu-MT">
                                                {{app()->isLocale('ar') ?$category->name_ar:$category->name_en}}
                                            </a>

                                            <div class="dubizzle_menu_dropdown dubizzle_menu_dropdown_MT"
                                                 @if($loop->last)  @if(app()->isLocale('en')) style="right: 0px"
                                                 @else style="left: 0px" @endif   @endif >
                                                <div class="box-flex">
                                                    <ul class="dubizzle_menu_dropdown_col dubizzle_menu_dropdown_col_childrens">

                                                        @if($category->children)

                                                            @foreach($category->children as $child)

                                                                <li class="dubizzle_menu_dropdown_item "
                                                                    data-submenu-id="menu-{{$child->id}}">
                                                                    <a href="@if(count($child->children??[])>0) {{url('products/'.$child->id)}}@else{{url('categories/'.$child->name_ar)}}  @endif"
                                                                       onmouseover="menueUpdate('{{$child->id}}')"
                                                                       class="dubizzle_menu_dropdown_item_link header_link "
                                                                       title="{{app()->isLocale('ar') ?$child->name_ar:$child->name_en}}"
                                                                       data-tr-event-name="click_category_selected"
                                                                       data-tr-context="{'cL1Id': 7, 'cL2Id': 140, 'cL3Id': '', 'cL4Id': '', 'categoryId': {{$child->id}} }">
                                                                        {{app()->isLocale('ar') ?$child->name_ar:$child->name_en}}
                                                                    </a>
                                                                    @if(count($child->children??[])>0)
                                                                        <div id="menu-{{$child->id}}"
                                                                             @if($loop->parent->last)  @if(app()->isLocale('en')) style="right: 100%;left:unset"
                                                                             @else style="left: 100%;right:unset"
                                                                             @endif   @endif class="children_dropdown"
                                                                             style="display: none;">
                                                                            <header class="children_dropdown_header">
                                                                                <span class="children_dropdown_header_title">{{app()->isLocale('ar') ?$child->name_ar:$child->name_en}}</span>
                                                                                <a href="{{url('categories/'.$child->name_ar)}}"
                                                                                   data-tr-event-name="all_in_category_selected"
                                                                                   class="children_dropdown_link">{{trans('app.see-all-ads')}}

                                                                                </a>
                                                                            </header>
                                                                            <ul class="children_dropdown_list children_dropdown_list_premium">
                                                                                @foreach($child->children as $sub_child)
                                                                                    <li class="children_dropdown_list_item">
                                                                                        <a href="{{url('categories/'.$sub_child->name_ar)}}"
                                                                                           title=" {{app()->isLocale('ar') ?$sub_child->name_ar:$sub_child->name_en}}"
                                                                                           data-tr-event-name="click_cat3_selected"> {{app()->isLocale('ar') ?$sub_child->name_ar:$sub_child->name_en}}</a>
                                                                                    </li>
                                                                                @endforeach
                                                                            </ul>
                                                                        </div>
                                                                    @endif
                                                                </li>
                                                            @endforeach
                                                        @endif


                                                    </ul>
                                                </div>
                                            </div>
                                        </li>


                                    @endif
                                @endforeach
                            @endif
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </header>
</div>
<script>
    function menueUpdate(id) {
        $('.children_dropdown').hide(200);
        $('#menu-' + id).show(200);
    }


</script>
@push('js')
    <script>

        function UpdateCountry(id, flag) {
            var country_id = id;
            if (country_id) {
                $.ajax({
                    url: '/information/create/ajax-countries?country_id=' + country_id + '&flag=' + flag,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $('#city_area').empty();
                        $.each(data, function (i, m) {
                            $('#city_area').append('<li onclick="UpdateCity(' + m.id + ')"><a href="javascript:;"> ' + m.name_ar + '</a></li>');
                        });
                        document.getElementById("header_flag_source").src = flag;
                        @php
                            session()->remove('city_id')
                        @endphp
                        location.reload();
                    }
                });

                $.ajax({
                    url: '/change-country?country_id=' + country_id,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {

                    },

                });

            } else {
                $('#header_city_id').empty();
            }
        }

        function UpdateCity(id) {
            $.ajax({
                url: '/change-city?city_id=' + id,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    $('.box-city').slideToggle();
                    $('.overlay-citys').fadeToggle();

                }
            });
            location.reload();
        }

    </script>
@endpush