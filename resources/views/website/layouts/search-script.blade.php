<script>

    var parent_cat = 0
    $('#subcategory_id').on('change', function () {
        $('#options_div').hide(200);
        $('#options_div').html('');
        var lang=$(this).attr('lang');
        parent_cat = this;
        document.getElementById('type').value = '';
        if ($(this).val()) {
            LoadOptions(this,lang);
        }
    });


    $('#type').on('change', function () {
        $('#options_div').hide(200);
        $('#options_div').html('');
        var lang=$(this).attr('lang');

        if ($(this).val()) {
            LoadOptions(this,lang);
        }
    });



    function LoadOptions(category,lng) {
        console.log(category)
        let subcategory =  document.getElementById('type').value ;
        let Maincategory =  document.getElementById('subcategory_id').value ;
        let url = '{{url('api/get-category-options/')}}?category[]=' + Maincategory +'&category[]=' +subcategory;
        $.ajax({
            url: url,
            beforeSend: function (request) {
                request.setRequestHeader("Accept-Language", lng);
            },
        }).done(function (data) {
            if (data.length > 0) {
                $('#options_div').show(200);
            }
            $.each(data, function (index, val) {
                let newdiv = document.createElement('div');
                newdiv.setAttribute("class", "option_dev");
                let values = [];
                $.each(val.option, function (index, value) {
                    values.push('<option value="' + value.id + '">' + value.name + '</option>')
                });
                newdiv.innerHTML = "<input type='hidden' readonly name='option_id' class='option_ids' value=" + val.id + ">" +
                    "<select class='form-control option_values' name='value'><option value='' >" + val.name + " </option>" +
                    values +
                    "</select>";
                document.getElementById('options_div').prepend(newdiv);
            });

            // if (parent_cat == 91 || parent_cat == 95 || parent_cat == 99) {
            //     let newdiv1 = document.createElement('tr');
            //     var str1 = '';
            //     str1 += "<td>#<input type='hidden' readonly ></td>" +
            //         "<td>" + "كيلومتر" + "</td>" +
            //         "<td>";
            //     str1 += "<input type='text' class='form-control' name='kilometer'> " +
            //         "</td>";
            //
            //     newdiv1.innerHTML = str1;
            //     document.getElementById('table_dynamic').appendChild(newdiv1);
            // }
        });
    }
</script>
