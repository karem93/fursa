@extends("website.layouts.app")
@section('content')
    <div class="container hide-in-sm">
        <section class="banner" id="top">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 pull-left">
                        <div class="banner-caption">
                            <div class="line-dec"></div>
                            <span style="margin:0 149px">{{trans('site.nav_title')}}</span>
                            <!-- <div class="add-adv">
                                <a href="#">اضف اعلانك الان</a>
                            </div> -->
                        </div>
                        <div class="submit-form">
                            <div class="tab-content" id="pills-tabContent">

                                <div class="tab-pane fade in active" id="pills1" role="tabpanel"
                                     aria-labelledby="pills-contact-tab">
                                    <form action="/search" method="post">
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-9 first-item" style="border-right:none">
                                                <fieldset>
                                                    <input name="search" type="text" class="form-control" id="name"
                                                           placeholder="{{trans('site.search')}}">
                                                </fieldset>
                                            </div>

                                            <div class="col-md-3">
                                                <fieldset>
                                                    <button type="submit" id="form-submit"
                                                            class="btn">{{trans('site.search')}}</button>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="popular-places" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>{{trans('site.category_details')}}</h2>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-3 col-xs-4">
                    <div class="service-item">
                        <h4>
                            @if($category->icon)
                                <img src="{{$category->icon}}" alt="" width="16" height="16">
                            @endif
                            <a href="#">{{$current_lang == 'ar' ?$category->name_ar: $category->name_en}}</a>
                        </h4>

                        <ul>
                            @foreach($category->children as $child)
                                <li><a href="/products/{{$child->id}}"><i
                                                class="fa fa-angle-left"></i>{{$current_lang == 'ar' ?$child->name_ar: $child->name_en}}
                                    </a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


