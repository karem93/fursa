@extends("website.layouts.app")
@section('content')


    @push('css')
        <link rel="stylesheet" href="{{asset('website/js/dropzone')}}/dropzone.css">
        <link rel="stylesheet" href="{{asset('website/js/dropzone')}}/basic.css">
    @endpush
    <div class="add-adv1 addv4">
        <div class="container">
            <h4>» {{trans('site.add_product')}} </h4>
            <br>
            <form class="basic-form" id="basic-form" method="post" action="{{ url('add-ad') }}"
                  enctype="multipart/form-data" autocomplete="off">
                @csrf
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">{{trans('site.choose_category')}}</label>
                    <div class="col-sm-10">
                        <select class="form-control" required id="category_id" name="category_id"
                                lang="{{app()->getLocale()}}">
                            <option value="" selected="selected">{{trans('site.choose_category')}}</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{app()->isLocale('ar') ?$category->name_ar:$category->name_en}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" id="ad_typelbl"
                           class="col-sm-2 col-form-label">{{trans('site.choose_ad_type')}}</label>
                    <div class="col-sm-10">
                        <select class="form-control" required id="subcategory_id" name="subcategory_id"
                                lang="{{app()->getLocale()}}">
                            <option value="" selected="selected"
                                    id="ad_typeopt">{{trans('site.choose_ad_type')}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" id="ad_descrlbl"
                           class="col-sm-2 col-form-label">{{trans('site.choose_ad_descr')}}</label>
                    <div class="col-sm-10">
                        <select class="form-control" required id="type" name="type" lang="{{app()->getLocale()}}">
                            <option value="" selected="selected"
                                    id="ad_descropt">{{trans('site.choose_ad_descr')}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group  row" id="options_div" style="display: none">
                    <label class="col-sm-2 col-form-label">{{trans('site.category_option')}} </label>
                    <div class="col-sm-10">
                        <div class="table-responsive">
                            <table class="table m-0">
                                <thead>
                                <tr>
                                    <th width="10%">#</th>
                                    <th width="10%">{{trans('site.option')}}</th>
                                    <th>{{trans('site.value')}}</th>
                                </tr>
                                </thead>
                                <tbody id="table_dynamic">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="form-group  row">
                    <label for="city_id" class="col-sm-2 col-form-label">{{trans('site.country')}} </label>
                    <div class="col-sm-10{{ $errors->has('main_category') ? ' has-danger' : '' }}">
                        <select name="country_id" id="country_id" class="form-control country_id m-input"
                                lang="{{app()->getLocale()}}" required>
                            <option value="">{{trans('site.country')}}</option>
                            @foreach($countries as $country)
                                <option
                                        value="{{$country->id}}" {{old('city_id')==$country->id ?'selected':''}}>{{ app()->isLocale('ar') ?$country->name_ar:$country->name_en }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group  row">
                    <label for="city_id" class="col-sm-2 col-form-label">{{trans('site.city')}} </label>
                    <div class="col-sm-10{{ $errors->has('main_category') ? ' has-danger' : '' }}">
                        <select name="city_id" id="city_id" class="form-control city_id m-input" required>
                        </select>
                    </div>
                </div>

                <div class="form-group  row">
                    <label class="col-sm-2 col-form-label">{{trans('site.ad_name')}} </label>
                    <div class="col-sm-10{{ $errors->has('title') ? ' has-danger' : '' }}">
                        {!! Form::text('title',null,['class'=>'form-control m-input','required','autofocus','placeholder'=>trans('site.ad_name')]) !!}
                    </div>
                </div>

                <div class="form-group  row">
                    <label class="col-sm-2 col-form-label">{{trans('site.phone')}} </label>
                    <div class="col-sm-10{{ $errors->has('phone') ? ' has-danger' : '' }}">
                        {!! Form::number('phone',null,['class'=>'form-control m-input','required','autofocus','placeholder'=>trans('site.phone')]) !!}
                    </div>
                </div>

                <div class="form-group row" id="price">
                    <label class="col-sm-2 col-form-label" id="ad_pricelbl">{{trans('site.price')}} </label>
                    <div class="col-sm-10{{ $errors->has('price') ? ' has-danger' : '' }}">
                        {!! Form::number('price',null,['class'=>'form-control m-input','id' => 'price_val','autofocus','required','placeholder'=>trans('site.price')]) !!}
                    </div>
                </div>


                <div class="form-group  row">
                    <label class="col-sm-2 col-form-label">{{trans('site.description')}}</label>
                    <div class="col-sm-10{{ $errors->has('description') ? ' has-danger' : '' }}">
                        {!! Form::textarea('description',null,['class'=>'form-control m-input','autofocus','required','placeholder'=>trans('site.description')]) !!}
                    </div>
                </div>

                <div class="form-group  row">
                    <input type="hidden" id="images" name="images">
                    <label class="col-sm-2 col-form-label">{{trans('site.images')}}</label>

                    <div class="col-sm-10 dropzone" id="mydropzone">
                        <div class="fallback">
                            <input type="file" name="file"/>
                            @csrf
                        </div>
                        <p>
                            <small>{{trans('site.to_add_image')}}</small><br/>
                            <small> {{trans('site.image_types')}}</small><br/>
                            <small> {{trans('site.image_size')}} </small>
                        </p>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="submit">{{trans('site.send')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script>

        $('#category_id').on('change', function (e) {
            var category_id = e.target.value;
            var lang = $(this).attr('lang');
            if ($(this).val() == 85) {
                $('#price').hide(200);
            } else {
                $('#price').show(200);
            }
            if (category_id == 33) {

                // سيارات
                if (lang == 'ar') {
                    $('#ad_typelbl').text('القسم الفرعى');
                    $('#ad_typeopt').text('اختر القسم الفرعى');
                    $('#ad_descrlbl').text('اختر الماركة');
                    $('#ad_descropt').text('اختر الماركة');
                } else {
                    $('#ad_typelbl').text('Subcategory');
                    $('#ad_typeopt').text('Choose Subcategory');
                    $('#ad_descrlbl').text('Make');
                    $('#ad_descropt').text('Choose Make');
                }

            }
            if (category_id == 50) {
                //عقارات
                if (lang == 'ar') {
                    $('#ad_typelbl').text('القسم الفرعى');
                    $('#ad_typeopt').text('اختر القسم الفرعى');
                    $('#ad_descrlbl').text('بيع او ايجار');
                    $('#ad_descropt').text('اختر');
                } else {
                    $('#ad_typelbl').text('Subcategory');
                    $('#ad_typeopt').text('Choose Subcategory');
                    $('#ad_descrlbl').text('Sell or rent ');
                    $('#ad_descropt').text('Choose');

                }

            }
            if (category_id == 64) {
                //اثاث المنزل
                if (lang == 'ar') {
                    $('#ad_typelbl').text('القسم الفرعى');
                    $('#ad_typeopt').text('اختر القسم الفرعى');
                    $('#ad_descrlbl').text('نوع الاثاث');
                    $('#ad_descropt').text('اختر نوع الاثاث');
                } else {
                    $('#ad_typelbl').text('Subcategory');
                    $('#ad_typeopt').text('Choose Subcategory');
                    $('#ad_descrlbl').text('Type of furniture ');
                    $('#ad_descropt').text('Choose Type of furniture ');

                }

            }
            if (category_id == 107) {
                //اجهزه
                if (lang == 'ar') {
                    $('#ad_typelbl').text('القسم الفرعى');
                    $('#ad_typeopt').text('اختر القسم الفرعى');
                    $('#ad_descrlbl').text('نوع الجهاز');
                    $('#ad_descropt').text('اختر نوع الجهاز');
                } else {
                    $('#ad_typelbl').text('Subcategory');
                    $('#ad_typeopt').text('Choose Subcategory');
                    $('#ad_descrlbl').text('Type of device ');
                    $('#ad_descropt').text('Choose Type of device ');

                }

            }
            if (category_id == 112) {
                //الوظائف
                if (lang == 'ar') {
                    $('#ad_typelbl').text('القسم الفرعى');
                    $('#ad_typeopt').text('اختر القسم الفرعى');
                    $('#ad_descrlbl').text('المسمى المظيفى ');
                    $('#ad_descropt').text('اختر المسمى الوظيفى');
                    $('#ad_pricelbl').text('الراتب');
                    $('#price_val').attr("placeholder", "ادخل الراتب");
                } else {
                    $('#ad_typelbl').text('Subcategory');
                    $('#ad_typeopt').text('Choose Subcategory');
                    $('#ad_descrlbl').text('Job Title');
                    $('#ad_descropt').text('Choose Job Title');
                    $('#ad_pricelbl').text('salary');
                    $('#price_val').attr("placeholder", "Enter Salary");
                }

            }
            if (category_id == 109 || category_id == 110 || category_id == 111) {
                //الاسر المنتجه والمجتمع والمللابس
                if (lang == 'ar') {
                    $('#ad_typelbl').text('القسم الفرعى');
                    $('#ad_typeopt').text('اختر القسم الفرعى');
                    $('#ad_descrlbl').text('النوع ');
                    $('#ad_descropt').text('اختر النوع');
                } else {
                    $('#ad_typelbl').text('Subcategory');
                    $('#ad_typeopt').text('Choose Subcategory');
                    $('#ad_descrlbl').text('Type');
                    $('#ad_descropt').text('Choose The Type');

                }

            }


        });

        var parent_cat = 0
        $('#subcategory_id').on('change', function (e) {
            $('#options_div').hide(200);
            $('#table_dynamic').html('');
            var lang = $(this).attr('lang');
            var category = e.target.value;
            parent_cat = e.target.value;
            document.getElementById('type').value = '';
            if (category) {
                LoadOptions(category, lang);
            }
        });

        $('#type').on('change', function (e) {
            $('#options_div').hide(200);
            $('#table_dynamic').html('');
            var lang = $(this).attr('lang');
            var category = e.target.value;
            if (category) {
                LoadOptions(category, lang);
            }
        });

        function LoadOptions(category, lang) {
            let subcategory = document.getElementById('type').value;
            let Maincategory = document.getElementById('subcategory_id').value;
            let url = '{{url('api/get-category-options/')}}?category[]=' + Maincategory + '&category[]=' + subcategory;

            $.ajax({
                url: url,
                beforeSend: function (request) {
                    request.setRequestHeader("Accept-Language", lang);
                },
            }).done(function (data) {
                if (data.length > 0) {
                    $('#options_div').show(200);
                }
                $.each(data, function (index, val) {
                    let newdiv = document.createElement('tr');

                    let values = [];
                    $.each(val.option, function (index, value) {
                        if (lang == 'ar')
                            values.push('<option value="' + value.id + '">' + value.name + '</option>')
                        else
                            values.push('<option value="' + value.id + '">' + value.name_en + '</option>')
                    });
                    var str = '';
                    str += "<td>#<input type='hidden' readonly name='option_id[]' value=" + val.id + "></td>" +
                        "<td>" + val.name + "</td>" +
                        "<td>";
                    if (lang == 'ar') {
                        str += "<select class='form-control' name='value[]'><option value='' >قم بالاختيار </option>" +
                            values +
                            "</select></td>";
                    } else {
                        str += "<select class='form-control' name='value[]'><option value='' >Choose Item </option>" +
                            values +
                            "</select></td>";
                    }


                    newdiv.innerHTML = str;
//                    newdiv.innerHTML = "<td>#<input type='hidden' readonly name='option_id[]' value=" + val.id + "></td>" +
//                        "<td>" + val.name + "</td>" +
//                        "<td><input name='value[]' class='form-control' type='text' ></td>";
                    document.getElementById('table_dynamic').prepend(newdiv);
                });

                if (parent_cat == 91 || parent_cat == 95 || parent_cat == 99) {
                    let newdiv1 = document.createElement('tr');
                    var str1 = '';
                    if (lang == 'ar') {
                        str1 += "<td>#<input type='hidden' readonly ></td>" +
                            "<td>" + "كيلومترات" + "</td>" +
                            "<td>";
                    } else {
                        str1 += "<td>#<input type='hidden' readonly ></td>" +
                            "<td>" + "Kilometers" + "</td>" +
                            "<td>";
                    }
                    str1 += "<input type='text' class='form-control' name='kilometer'> " +
                        "</td>";

                    newdiv1.innerHTML = str1;
                    document.getElementById('table_dynamic').appendChild(newdiv1);
                }

                if (parent_cat == 51 || parent_cat == 52 || parent_cat == 53 || parent_cat == 54 || parent_cat == 55 || parent_cat == 56 || parent_cat == 327) {
                    let newdiv1 = document.createElement('tr');
                    var str1 = '';
                    if (lang == 'ar') {
                        str1 += "<td>#<input type='hidden' readonly ></td>" +
                            "<td>" + "المساحة" + "</td>" +
                            "<td>";
                    } else {
                        str1 += "<td>#<input type='hidden' readonly ></td>" +
                            "<td>" + "Space" + "</td>" +
                            "<td>";
                    }
                    str1 += "<input type='text' class='form-control' name='space'> " +
                        "</td>";

                    newdiv1.innerHTML = str1;
                    document.getElementById('table_dynamic').appendChild(newdiv1);
                }
            });
        }
    </script>
    <script src="{{asset('website/js/dropzone/dropzone.js')}}"></script>

    <script>

        jQuery(document).ready(function () {
            Dropzone.autoDiscover = false;
            let images = [];


            $("div#mydropzone").dropzone({
                url: "{{url('add-ad/image-ajax-upload')}}",
                rtl: true,
                maxFilesize: 1,
                maxFiles: 20,
                timeout: 5000,
//                dictDefaultMessage: 'قم بسحب وافلات الصور هنا للرف',
                init: function () {
                    this.on("sending", function (file, xhr, formData) {
                        formData.append("_token", "{{csrf_token()}}");
                    });
                },
                success: function (file, response) {
                    images.push(response);
                    $("input#images").val(JSON.stringify(images))
                },
            });
            let Delete = function (id) {
                $("#loaderDiv").show();
                $.ajax({

                    type: "POST",
                    url: "{{url('account/ads/images/Delete')}}",
                    data: {id: id, _token: '{{csrf_token()}}'},
                    success: function (result) {
                        $("#loaderDiv").hide();
                        $("#ExprojDel").modal("hide");
                        $("#image_" + id).remove();
                    }
                })
            };
        });
    </script>

@endpush

