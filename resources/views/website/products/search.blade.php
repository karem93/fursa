@extends("website.layouts.app")
@section('content')


    @if(Helpers::ReturnAds(1)->status ==1)

        @if(Helpers::ReturnAds(1)->type ==1)
            <section>
                <div class="container">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">

                            <div class="item active">
                                <img src="{{Helpers::ReturnAds(1)->path}}" alt="Los Angeles"
                                     style="width:100%; height: 250px">
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        @else
            <div>
                <div class="container" style="text-align: center">

                    {!! Helpers::ReturnAds(1)->code !!}
                </div>
            </div>
        @endif
    @endif
    <div class="container hide-in-sm">
        <section class="banner" id="top">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 pull-left">
                        <div class="banner-caption">
                            <div class="line-dec"></div>
                            <span style="margin:0 149px">{{trans('site.nav_title')}}</span>
                            <!-- <div class="add-adv">
                                <a href="#">اضف اعلانك الان</a>
                            </div> -->
                        </div>
                        <div class="submit-form">

                            <div class="tab-content" id="pills-tabContent">

                                <div class="tab-pane fade in active" id="pills1" role="tabpanel"
                                     aria-labelledby="pills-contact-tab">
                                    <form action="/search" method="post">
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-9 first-item" style="border-right:none">
                                                <fieldset>
                                                    <input name="search" type="text" class="form-control" id="name"
                                                           placeholder="{{trans('site.search')}}">
                                                </fieldset>
                                            </div>

                                            <div class="col-md-3">
                                                <fieldset>
                                                    <button type="submit" id="form-submit"
                                                            class="btn">{{trans('site.search')}}</button>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <br>
    <section class="popular-places" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                    <div class="col-md-12 filter">

                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                        <div class="form-group row ">
                            <div class="col-sm-12">
                                <select class="form-control" id="city_id" name="city_id" lang="{{app()->getLocale()}}">>
                                    <option value="" selected="selected">{{trans('site.all_cities')}}</option>
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}" {{session('city_id')==$city->id?'selected':''}}>{{app()->isLocale('ar') ?$city->name_ar:$city->name_en}}</option>
                                    @endforeach

                                </select>
                            </div>

                            <div class="col-sm-12">
                                <select class="form-control" id="category_id" name="category_id"
                                        lang="{{app()->getLocale()}}">
                                    <option value="" selected="selected">{{trans('site.choose_category')}}</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{app()->isLocale('ar') ?$category->name_ar:$category->name_en}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="col-sm-12">
                                <select class="form-control" id="subcategory_id" name="subcategory_id"
                                        lang="{{app()->getLocale()}}">
                                    <option selected="selected" value="">{{trans('site.choose_ad_type')}}</option>

                                </select>
                            </div>


                            <div class="col-sm-12">
                                <select class="form-control" id="type" name="type" lang="{{app()->getLocale()}}">
                                    <option selected="selected" value="">{{trans('site.choose_ad_descr')}}</option>

                                </select>
                            </div>
                            <div class="col-sm-12" id="options_div" style="display: none">

                            </div>

                        </div>
                        <div class="form-group row">
                            <label for="price" class="col-sm-12 col-form-label"
                                   id="pricelbl">{{trans('site.price')}}</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="price_start" name="price_start"
                                       placeholder="{{trans('site.from')}}">
                            </div>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="price_end" name="price_end"
                                       placeholder="{{trans('site.to')}}">
                            </div>
                        </div>


                        <br>


                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button class="btn btn-primary" id="searchFilter"
                                        lang="{{app()->getLocale()}}"
                                        currency={{ Helpers::getCurrency() }}>{{trans('site.search')}}</button>
                            </div>
                        </div>


                    </div>


                    <div class="col-md-12">
                        @if(Helpers::ReturnAds(4)->status ==1)

                            @if(Helpers::ReturnAds(4)->type ==1)
                                <section>
                                    <div class="">
                                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">

                                                <div class="item active">
                                                    <img src="{{Helpers::ReturnAds(4)->path}}" alt="Los Angeles"
                                                         style=" height: 250px">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </section>
                            @else
                                <div>
                                    <div class="container" style="text-align: center">

                                        {!! Helpers::ReturnAds(4)->code !!}
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="section-heading">
                        <h2>{{trans('site.search_results')}}</h2>
                        <br>

                        @if($products->isNotEmpty())
                            <div class="" dir="ltr" id="search_results">
                                @foreach($products as $product)
                                    <div class="list-item-wrapper col-md-12" id="list-item-wrapper">
                                        <div class="cf item">
                                            <div class="listing-item" data-id="10970873" data-score="0">
                                                <div class="block item-title">
                                                    <h2 id="title" class="results-listing-title">
                                                        <span class="title">
                                                             <a href="/product/{{$product->id}}">
                                                                     {{$product->title}}
                                                             </a>
                                                        </span>
                                                    </h2>
                                                    <div class="price">
                                                        @if(app()->getLocale() == 'en')
                                                            <span class="selling-price__amount">{{$product->price}} {{ Helpers::getCurrency() }}<br></span>
                                                        @else
                                                            <span class="selling-price__amount">{{ Helpers::getCurrency() }} {{$product->price}} <br></span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="block has_photo">
                                                    <div class="thumb">
                                                        <a href="/product/{{$product->id}}">
                                                            <div style="background-image:url('{{count($product->ProductImage) > 0 ? $product->ProductImage[0]->image : ''}}');"></div>
                                                        </a>
                                                    </div>
                                                    <div class="description ">
                                                        <ul class="feature">
                                                            <li>
                                                                <p class="date">{{$product->created_at}}</p>

                                                            </li>
                                                            <li class="features__container">
                                                                <ul class="features">

                                                                    @foreach($product->ProductOption as $option)
                                                                        @if(app()->getLocale() == 'ar')
                                                                            <li>
                                                                                @if($option->option->icon)<img
                                                                                        src="{{$option->option->icon}}"
                                                                                        width="25px"
                                                                                        height="25px">@endif
                                                                                <strong>{{app()->isLocale('ar') ? $option->option->name_ar : $option->option->name_en }}
                                                                                    : {{app()->isLocale('ar') ? ( $option->optionValue ? $option->optionValue->value : '------------') : ( $option->optionValue ? $option->optionValue->value_en : '---------')}}
                                                                                </strong>


                                                                                &nbsp;&nbsp;
                                                                            </li>
                                                                        @else
                                                                            <li style="    width: 33.3%;    margin-bottom: 5px;">
                                                                                @if($option->option->icon)<img
                                                                                        src="{{$option->option->icon}}"
                                                                                        width="25px"
                                                                                        height="25px">@endif


                                                                                <strong>{{app()->isLocale('ar') ? $option->option->name_ar : $option->option->name_en }}
                                                                                    : {{app()->isLocale('ar') ? ($option->optionValue ? $option->optionValue->value : '------------'):($option->optionValue ? $option->optionValue->value_en : '---------')}}
                                                                                </strong>
                                                                                &nbsp;&nbsp;
                                                                            </li>
                                                                        @endif
                                                                    @endforeach
                                                                </ul>

                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <br class="clear">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <span class="text-danger text-bold">{{trans('site.no_results')}}</span>
                        @endif

                    </div>
                </div>
            </div>
            @if(Helpers::ReturnAds(3)->status ==1)

                @if(Helpers::ReturnAds(3)->type ==1)
                    <section>
                        <div class="container">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">

                                    <div class="item active">
                                        <img src="{{Helpers::ReturnAds(3)->path}}" alt="Los Angeles"
                                             style="width:100%; height: 250px">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </section>
                @else
                    <div>
                        <div class="container" style="text-align: center">

                            {!! Helpers::ReturnAds(3)->code !!}
                        </div>
                    </div>
                @endif
            @endif

        </div>
    </section>
@endsection
@push('js')
    @include('website.layouts.search-script')
@endpush
