@extends("website.layouts.app")
@section('content')
    <div class="login-page">
        <div class="container">
            <div class="col-md-6">

                <h3>{{trans('site.register')}}</h3>

                <div class="login-form">
                    <form action="{{route('register')}}" method="post">
                        @csrf
                        <div class="form-g">
                            <input type="text" name="phone" value="{{old('phone')}}" placeholder="{{trans('site.phone')}}">
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('phone') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="text" name="username" value="{{old('username')}}" placeholder="{{trans('site.username')}}">
                            @if ($errors->has('username'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('username') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="email" name="email" value="{{old('email')}}" placeholder="{{trans('site.email')}}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('email') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="password" name="password"  placeholder="{{trans('site.password')}}">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('password') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="password" name="password_confirmation"  placeholder="{{trans('site.confirm_password')}}">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('password_confirmation') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <select name="country_id" id="country_id" class="form-control" lang="{{app()->getLocale()}}" placeholder="{{trans('site.country')}}">
                                <option value=""> {{trans('site.country')}}</option>
                                @foreach($countries as $country)
                                    <option value="{{$country->id}}"> {{app()->isLocale('ar') ?$country->name_ar:$country->name_en}}</option>

                                @endforeach

                            </select>
                            @if ($errors->has('country_id'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('country_id') }}
                                   </strong>
                                </span>
                            @endif
                        </div><br>
                        <div class="form-g">
                            <select name="city_id" id="city_id" class="form-control">


                            </select>
                            @if ($errors->has('city_id'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('city_id') }}
                                   </strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-g">
                            <button type="submit">{{trans('site.register')}}</button>
                        </div>
                    </form>
                </div>
            </div>


            <div class="col-md-6">

                <h3>{{trans('site.website_register')}}</h3>

                <div class="login-form register-text">
                    {{trans('site.website_register_text')}}
                </div>
            </div>


        </div>
    </div>

@endsection
