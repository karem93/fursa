<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم القسم بالعربية: </label>
    <div class="col-lg-10{{ $errors->has('name_ar') ? ' has-danger' : '' }}">
        {!! Form::text('name_ar',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم القسم بالعربية"]) !!}
        @if ($errors->has('name_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_ar') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم القسم بالانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('name_en') ? ' has-danger' : '' }}">
        {!! Form::text('name_en',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم القسم بالانجليزية"]) !!}
        @if ($errors->has('name_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_en') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">القسم الرئيسي </label>
    <div class="col-lg-10{{ $errors->has('parent_id') ? ' has-danger' : '' }}">
        <select name="parent_id" id="" class="form-control m-input--solid">
            <option value="">اختر القسم الرئيسي</option>
            @foreach($categories as $main_category)
                <option value="{{$main_category->id}}"
                        @if(isset($category) && $main_category->id == $category->parent_id) selected @endif>{{$main_category->name_ar}}</option>
            @endforeach
        </select>
        <label class="col-lg-10 form-control-label">يمكنك اختار قسم رئيسي في حالة اضافة قسم فرعي </label>
        @if ($errors->has('status'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>




<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">خصائص القسم</label>
    <div class="col-lg-10{{ $errors->has('parent_id') ? ' has-danger' : '' }}">
        <div class="table-responsive">
            <table class="table m-0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم بالعربيه</th>
                    <th>الاسم بالانجليزيه</th>
                    <th>الايقونه</th>
                    <th> <a href="javascript:void(0)" onClick="addInput('table_dynamic');" class="btn btn-sm  btn-dark float-left"><span class="fa fa-plus fa-2x"></span></a></th>
                </tr>
                </thead>
                <tbody id="table_dynamic">
                @foreach($option ??[] as $opt)
                <tr>
                    <td><input type="hidden" name="option_id[]" value="{{$opt->id}}">#</td>
                    <td><input name="title_ar[]"  class="form-control" value="{{$opt->name_ar}}" type="text" ></td>
                    <td><input name="title_en[]" class="form-control"  value="{{$opt->name_en}}" type="text" ></td>
                    <td>
                        @if($opt->icon)
                        <img src="{{$opt->icon}}" width="100px" height="100px">
                        @endif
                        <input type="file" name="option_icon[]" class="form-control ">

                    </td>

                    <td></td>
                </tr>

                @endforeach

                <tr>
                    <td>#</td>
                    <td><input name="title_ar[]"  class="form-control" type="text"></td>
                    <td><input name="title_en[]" class="form-control" type="text"></td>
                    <td>
                        <input type="file" name="option_icon[]" class="form-control ">

                    </td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>


    </div>
</div>







{{--<div class="form-group m-form__group row">--}}
    {{--<label class="col-lg-2 col-form-label">الايقونة: </label>--}}
    {{--<div class="col-lg-10{{ $errors->has('icon') ? ' has-danger' : '' }}">--}}

        {{--<input type="file" name="icon" class="form-control uploadinput">--}}
        {{--@if ($errors->has('icon'))--}}
            {{--<span class="form-control-feedback" role="alert">--}}
                {{--<strong>{{ $errors->first('icon') }}</strong>--}}
            {{--</span>--}}
        {{--@endif--}}
    {{--</div>--}}

{{--</div>--}}



{{--@if(isset($category) && $category->icon)--}}
    {{--<div class="row">--}}
        {{--<div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">--}}
            {{--<img--}}
                {{--data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"--}}
                {{--alt="First slide [800x4a00]"--}}
                {{--src="{{$category->icon}}"--}}
                {{--style="height: 150px; width: 150px"--}}
                {{--data-holder-rendered="true">--}}
        {{--</div>--}}
    {{--</div>--}}
{{--@endif--}}

@section('footer')
    <script type="text/javascript">
        let counter = 1;
        let limit = 99;
        function addInput(divName){
            if (counter == limit)  {
                alert("You have reached the limit of adding " + counter + " inputs");
            }
            else {
                let newdiv = document.createElement('tr');
                newdiv.innerHTML = "<td>#</td><td><input name='title_ar[]'  class='form-control' type='text' ></td>" +
                    "<td><input name='title_en[]' class='form-control' type='text' ></td>" +
                     "<td><input type='file' name='option_icon[]' class='form-control'></td>"+
                    "<td></td>";
                document.getElementById(divName).appendChild(newdiv);
                counter++;
            }
        }

    </script>
@endsection


