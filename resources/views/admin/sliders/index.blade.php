@extends('admin.layouts.app')
@section('title')
    البنرات الاعلانية
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item active-top-bar">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">الاعلانات</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>

@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        الاعلانات
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_country">
                <thead>
                <tr>
                    <th>الصوره</th>
                    <th>الموقع</th>
                    <th>النوع</th>
                    <th>الحاله</th>
                    <th>تعديل</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sliders as $index=>$slider)
                    <tr>
                        <td><img  width="120px;" height="120px;" src="{{$slider->path}}"></td>
                        <td>{{ Helpers::AdsLocation()[$slider->location]}}</td>
                        <td>{{$slider->type == 1?'صوره':'كود مضمن'}}</td>
                        <td>{{$slider->status == 1?'مفعل':'غير مفعل'}}</td>
                        <td>
                            <a title="تعديل" href="/webadmin/ads/{{$slider->id}}/edit"><i
                                    class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/countrcountriess!!}--}}
@endsection
