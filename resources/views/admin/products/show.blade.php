@extends('admin.layouts.app')

@section('title')
    تفاصيل المنتج
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/products')}}" class="m-menu__link">
            <span class="m-menu__link-text">المنتجات</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">تفاصيل المنتج</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تفاصيل المنتج
                    </h3>
                </div>
            </div>
        </div>


    <!--begin::Form-->
        {!! Form::model($product,['route' => ['products.show' , $product->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">



            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">اسم المنتج  </label>
                <div class="col-lg-5{{ $errors->has('title') ? ' has-danger' : '' }}">
                    {!! Form::text('title',old('title'),['class'=>'form-control m-input','autofocus','disabled' ]) !!}

                </div>
                <label class="col-lg-1 col-form-label">السعر</label>
                <div class="col-lg-5{{ $errors->has('price') ? ' has-danger' : '' }}">
                    {!! Form::text('price',old('price'),['class'=>'form-control m-input','disabled' ]) !!}

                </div>
                <label class="col-lg-1 col-form-label">التصنيف</label>
                <div class="col-lg-5{{ $errors->has('category') ? ' has-danger' : '' }}">
                    <input class='form-control m-input' disabled value="{{$product->category->parent->parent->name_ar}}-{{$product->category->parent->name_ar}}-{{$product->category->name_ar ?? ''}}" >
                </div>
                <label class="col-lg-1 col-form-label">عدد المشاهدات</label>
                <div class="col-lg-5{{ $errors->has('views') ? ' has-danger' : '' }}">
                    {!! Form::text('views',old('views'),['class'=>'form-control m-input','disabled' ]) !!}
                </div>
                <label class="col-lg-1 col-form-label">المستخدم</label>
                <div class="col-lg-5{{ $errors->has('user') ? ' has-danger' : '' }}">
                    <input class='form-control m-input' disabled value="{{$product->user->username ?? ''}}" >
                </div>
                <label class="col-lg-1 col-form-label">رقم الهاتف</label>
                <div class="col-lg-5{{ $errors->has('phone') ? ' has-danger' : '' }}">
                    {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','disabled' ]) !!}
                </div>

                <label class="col-lg-1 col-form-label">المدينه</label>
                <div class="col-lg-5{{ $errors->has('city') ? ' has-danger' : '' }}">
                    <input class='form-control m-input' disabled value="{{$product->city->name_ar ?? ''}}" >
                </div>
                <div class="col-lg-12"></div>


                <label class="col-lg-1 col-form-label">التفاصيل</label>
                <div class="col-lg-11{{ $errors->has('description') ? ' has-danger' : '' }}">
                    {!! Form::textarea('description',old('description'),['class'=>'form-control m-input','disabled' ]) !!}
                </div>


            </div>

            <div class="form-group m-form__group row" >
                <label class="col-lg-2 col-form-label">خصائص القسم: </label>
                <div class="col-lg-10">

                    <div class="table-responsive">
                        <table class="table m-0">
                            <thead>
                            <tr>
                                <th width="10%">#</th>
                                <th width="10%">الخاصيه</th>
                                <th>القيمه</th>
                            </tr>
                            </thead>
                            <tbody id="table_dynamic">


                            @foreach($product->ProductOption as $item)
                                <tr>
                                    <td>#</td>
                                    <td>{{$item->option->name_ar ?? ''}}</td>
                                    <td>{{$item->optionValue->value ?? ''}}</td>
                                </tr>



                            @endforeach


                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            @if(isset($product) && $product->ProductImage)
                <div class="row">
                    @foreach($product->ProductImage as $photo)
                        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

                            <img data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide" alt="First slide [800x4a00]" src="{{\Helpers::base_url().$photo->image}}" style="height: 150px; width: 150px" data-holder-rendered="true">
                        </div>
                    @endforeach
                </div>
            @endif








        </div>

    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

