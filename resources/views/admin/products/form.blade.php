<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم المنتج : </label>
    <div class="col-lg-10{{ $errors->has('title') ? ' has-danger' : '' }}">
        {!! Form::text('title',null,['class'=>'form-control m-input','required','autofocus','placeholder'=>"اسم المنتج"]) !!}
        @if ($errors->has('title'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">رقم الهاتف : </label>
    <div class="col-lg-10{{ $errors->has('phone') ? ' has-danger' : '' }}">
        {!! Form::number('phone',null,['class'=>'form-control m-input','required','autofocus','placeholder'=>"رقم الهاتف "]) !!}
        @if ($errors->has('phone'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">سعر المنتج : </label>
    <div class="col-lg-10{{ $errors->has('price') ? ' has-danger' : '' }}">
        {!! Form::number('price',null,['class'=>'form-control m-input','autofocus','required','placeholder'=>"سعر المنتج"]) !!}
        @if ($errors->has('price'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">وصف المنتج : </label>
    <div class="col-lg-10{{ $errors->has('description') ? ' has-danger' : '' }}">
        {!! Form::textarea('description',null,['class'=>'form-control m-input','autofocus','required','placeholder'=>"وصف المنتج"]) !!}
        @if ($errors->has('description'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>

</div>





<div class="form-group m-form__group row">
    <label for="city_id" class="col-lg-2 col-form-label">المدينه: </label>
    <div class="col-lg-10{{ $errors->has('main_category') ? ' has-danger' : '' }}">
        <select name="city_id" data-url="{{url('api/get-categories/')}}" id="city_id" class="form-control city_id m-input" required>
            <option value="">قم باختيار المدينه </option>
            @foreach($cities as $city)
                <option value="{{$city->id}}" {{isset($product) && $product->city_id==$city->id ?'selected':''}}>{{ $city->name_ar }}</option>
            @endforeach
        </select>
        @if ($errors->has('city_id'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('city_id') }}</strong>
            </span>
        @endif
    </div>
</div>




<div class="form-group m-form__group row">
    <label for="main_category" class="col-lg-2 col-form-label">القسم الرئيسي: </label>
    <div class="col-lg-10{{ $errors->has('main_category') ? ' has-danger' : '' }}">
            <select name="main_category" data-url="{{url('api/get-categories/')}}" id="category_id" class="form-control main_category m-input" required>
                <option value="">قم باختيار القسم الرئيسي</option>
                @foreach($categories as $category)
                    <option value="{{$category->id}}" {{isset($product) && $product->category->parent->parent->id==$category->id ?'selected':''}}>{{ $category->name_ar }}</option>
                @endforeach
            </select>
        @if ($errors->has('main_category'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('main_category') }}</strong>
            </span>
        @endif
    </div>

</div>








<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">نوع السلعه: </label>
    <div class="col-lg-10{{ $errors->has('subcategory_id') ? ' has-danger' : '' }}">
            <select name="subcategory_id"  id="subcategory_id"  class="form-control subcategory_id" required>
                <option value=""  >قم باختيار نوع السلعه</option>
                @if(isset($product))
                    @foreach($product->category->parent->parent->children as $category)
                        <option value="{{$category->id}}" {{isset($product) && $product->category->parent->id==$category->id ?'selected':''}}>{{ $category->name_ar }}</option>
                    @endforeach

                @endif
            </select>
        @if ($errors->has('subcategory_id'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('subcategory_id') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">مواصفات السلعه: </label>
    <div class="col-lg-10{{ $errors->has('type') ? ' has-danger' : '' }}">
        <select name="type"  id="type"  class="form-control type" required>
            <option value="" selected >قم باختيار مواصفات السلعه</option>
            @if(isset($product))
                @foreach($product->category->parent->children as $category)
                    <option value="{{$category->id}}" {{isset($product) && $product->category_id==$category->id ?'selected':''}}>{{ $category->name_ar }}</option>
                @endforeach

            @endif
        </select>
        @if ($errors->has('type'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('type') }}</strong>
            </span>
        @endif
    </div>

</div>


<div class="form-group m-form__group row" id="options_div" style="display: none">
    <label class="col-lg-2 col-form-label">خصائص القسم: </label>
    <div class="col-lg-10">

        <div class="table-responsive">
            <table class="table m-0">
                <thead>
                <tr>
                    <th width="10%">#</th>
                    <th width="10%">الخاصيه</th>
                    <th>القيمه</th>
                </tr>
                </thead>
                <tbody id="table_dynamic">
                </tbody>
            </table>
        </div>

    </div>

</div>

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الصوره: </label>
    <div class="col-lg-10{{ $errors->has('images') ? ' has-danger' : '' }}">

        <input type="file" name="images[]" multiple  class="form-control uploadinput">
        @if ($errors->has('images'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('images') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($product) && $product->ProductImage)
    <div class="row">
        @foreach($product->ProductImage as $photo)
        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

            <img data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide" alt="First slide [800x4a00]" src="{{\Helpers::base_url().$photo->image}}" style="height: 150px; width: 150px" data-holder-rendered="true">
        </div>
        @endforeach

    </div>
@endif
@push('js')
    <script>
        $('#main_category').on('change', function () {
            let url = $(this).data('url') + '/' + $(this).val();

            $('#options_div').hide(200);
            $('#table_dynamic').html('');
            $('.category_id').html('<option value="">قم باختيار القسم الفرعي</option>');

            if ($(this).val()){
                $.ajax({
                    url: url,
                    beforeSend: function(request) {
                        request.setRequestHeader("Accept-Language", 'ar');
                    },
                }).done(function (data) {
                    $('.category_id').html('<option value="">قم باختيار القسم الفرعي</option>');
                    let locals = $('.category_id');
                    locals.selectedIndex = 0;
                    $.each(data, function (index, val) {
                        locals.append('<option value=' + val.id + '>' + val.name + '</option>');
                    });
                });
            }
        });
        $('#subcategory_id').on('change', function () {
            $('#options_div').hide(200);
            $('#table_dynamic').html('');
            if ($(this).val()) {
                LoadOptions.call(this);
            }
        });

        function LoadOptions(category) {
            $('#options_div').show(200);
            $('#table_dynamic').html('');
            let url = '{{url('api/get-category-options/')}}/' + $(this).val();
            $.ajax({
                url: url,
                beforeSend: function(request) {
                    request.setRequestHeader("Accept-Language", 'ar');
                },
            }).done(function (data) {
                $.each(data, function (index, val) {
                    let values = [];
                    $.each(val.option, function (index, value) {
                        values.push('<option value="'+value.id+'">'+value.name+'</option>')

                    });
                    let newdiv = document.createElement('tr');
                    newdiv.innerHTML = "<td>#<input type='hidden' readonly name='option_id[]' value=" + val.id + "></td>" +
                        "<td>" + val.name + "</td>" +
                        "<td>" +
                        "<select class='form-control' name='value[]'><option value='' >قم بالاختيار </option>" +
                        values+
                        "</select></td>";
                    document.getElementById('table_dynamic').appendChild(newdiv);
                });
            });
        }

        @if(isset($product) && $product->category_id)
            window.onload = (event) => {
            $.ajax({
                url: '{{url('webadmin/get-product-category-data/')}}/' + '{{$product->id}}/{{$product->category_id}}',
                beforeSend: function(request) {
                    request.setRequestHeader("Accept-Language", 'ar');
                },
            }).done(function (data) {

                document.getElementById("main_category").value   = data.parent_id.parent_id;
                let locals = $('.category_id');
                locals.selectedIndex = 0;
                $.each(data.sup_category, function (index, val) {
                    locals.append('<option value=' + val.id + '>' + val.name_ar + '</option>');
                });
                console.log('{{$product->category_id}}')
                document.getElementById("category_id").value   = '{{$product->category_id}}';
                $('#options_div').show(200);
                $('#table_dynamic').html('');
                $.each(data.category_option, function (index, val) {
                    let values = [];
                    $.each(val.values2, function (index, value) {
                        var selected = '';
                        if (value.id == val.option_value_id){
                            selected = 'selected'
                        }
                        values.push('<option '+selected+' value="'+value.id+'">'+value.name+'</option>')
                    });
                    let newdiv = document.createElement('tr');
                    newdiv.innerHTML = "<td>#<input type='hidden' readonly name='option_id[]' value=" + val.id + "></td>" +
                        "<td>" + val.name_ar + "</td>" +
                        "<td>" +
                        "<select class='form-control' name='value[]'><option value='' >قم بالاختيار </option>" +
                        values+
                        "</select></td>";
                    document.getElementById('table_dynamic').appendChild(newdiv);
                });

            });
        };
        @endif



    </script>
@endpush






