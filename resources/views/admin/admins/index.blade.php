@extends('admin.layouts.app')
@section('title')
    المدراء
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/admins')}}" class="m-menu__link">
            <span class="m-menu__link-text">المدراء</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>


@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        المدراء
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div><a href="{{route('admins.create')}}" style="margin-bottom:20px"
                    class="btn btn_primary btn btn-danger"><i class=" fa fa-edit"></i>اضافة مدير</a></div>
            <br>


            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable search_result"
                   id="m_table_testArea">

                <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>الهاتف</th>
                    <th>البريد الالكترونى</th>
                    <th>الدوله</th>
                    <th>المدينه</th>
                    <th>الادوات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($admins as $index=> $user)
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$user->username}} </td>
                        <td>{{$user->phone}} </td>
                        <td>{{$user->email}} </td>
                        <td>{{$user->country->name_ar }} </td>
                        <td>{{$user->city->name_ar }} </td>
                        <td>


                            <a title="عرض" href="/webadmin/admins/{{$user->id}}"><i class="fa fa-eye"></i></a>
                            {{--<a  title="Bank Data" href="/webadmin/user/{{$user->id}}/bank-data" ><i class="fa fa-credit-card"></i></a>--}}
                            <a title="تعديل" href="/webadmin/admins/{{$user->id}}/edit"><i class="fa fa-edit"></i></a>
                            <form class="inline-form-style"
                                  action="/webadmin/admins/{{ $user->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/testArea/script.js') !!}--}}

@endsection
