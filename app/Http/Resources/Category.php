<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $has_sub = false;
        $categories = \App\Models\Category::where('parent_id', $this->id)->get();
        if (count($categories) > 0)
            $has_sub = true;
        return [
            'id' => $this->id,
            'name' => \Helpers::getLang() == 'ar' ? $this->name_ar : $this->name_en,
            'icon' => \Helpers::category_image($this->icon),
            'has_sub' => $has_sub
        ];
    }
}
