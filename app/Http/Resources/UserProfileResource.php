<?php

namespace App\Http\Resources;

use App\Models\Fav;
use Illuminate\Http\Resources\Json\JsonResource;

class  UserProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $my_products = \App\Models\Product::where('user_id', $this->id)->get();
        $my_products_views = \App\Models\Product::where('user_id', $this->id)->get()->sum('views');
        $my_favs = Fav::where('user_id', $this->id)->count();
        return [
            "id" => (int)$this->id,
            "username" => (string)$this->username,
            "email" => (string)$this->email,
            "image" => $this->photo ? \Helpers::base_url() . $this->photo : '',
            "phone" => (string)$this->phone,
            "country_id" => $this->country->id,
            "country" => App()->getLocale() == 'ar' ? $this->country->name_ar : $this->country->name_en,
            "city_id" => $this->city->id,
            "city" => App()->getLocale() == 'ar' ? $this->city->name_ar : $this->city->name_en,
            'my_products_count' => $my_products->count(),
            'views_count' => $my_products_views,
            'my_favs_count' => $my_favs,
            'products' => new ProductCollection($my_products),
        ];

    }
}
