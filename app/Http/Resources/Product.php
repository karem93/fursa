<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $photos = $this->ProductImage;
        $imgs = [];
        foreach ($photos as $photo) {
            $imgs[] = \Helpers::base_url().$photo->image;
        }
        return [
            'id' => $this->id,
            'title' => $this->title,
            'price' => (double)$this->price,
            'currency' => \Helpers::getCurrencyOfCountry(),
            'category' =>$this->category?( App()->getLocale() == 'ar' ? $this->category->name_ar : $this->category->name_en):'',
            'city' => $this->city?(App()->getLocale() == 'ar' ? $this->city->name_ar : $this->city->name_en):'',
            'photos' => $imgs,
            'is_fav' => $this->isFav(\Helpers::getLoggedUser()->id, $this->id)
        ];
    }
}
