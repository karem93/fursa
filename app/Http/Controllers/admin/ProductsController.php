<?php

namespace App\Http\Controllers\admin;

use App\Http\Resources\ProductDetails;
use App\Models\City;
use App\Models\Category;
use App\Models\Option;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductOption;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use function MongoDB\BSON\fromJSON;
use function MongoDB\BSON\toJSON;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $products  = DB::table('products as t1')
            ->leftJoin('categories as t2' ,'t1.category_id' , '=' , 't2.id')
            ->select('t1.id', 't1.title', 't1.price', 't2.name_ar', DB::raw('(select image from product_images where product_id = t1.id order by id asc limit 1) as image'))
            ->paginate(20);
        return view('admin.products.index', compact('products'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('parent_id', null)->get();
         $cities = City::get();
        return view('admin.products.add',compact('categories','cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        return $request;
        $request->validate([
            'title' => 'required|string|max:255',
            'phone' => 'required|string',
            'price' => 'required|string',
            'description' => 'sometimes|nullable|string',
            'type' => 'required',
            'city_id' => 'required',
            'images' => 'array',
            'images.*' => 'mimes:png,jpeg,jpg|max:8000',
            'options' => 'array',
        ]);
        $data = [
            'user_id' => auth()->id(),
            'title' => $request->title,
            'phone' => $request->phone,
            'price' => $request->price,
            'description' => $request->description,
            'category_id' => $request->type,
            'city_id' => $request->city_id,
        ];
        $images = request()->file('images');
        $images_pathes = [];

        foreach ($images as $key => $image) {
            $imageName = Str::random(10) . '.' . $image->extension();
            $image->move(
                base_path() . '/public/uploads/products/', $imageName
            );
            $images_pathes[$key] = '/uploads/products/' . $imageName;
        }

        $product = Product::create($data);
        $product_id = $product->id;
        foreach ($images_pathes as $key => $path) {
            $dataa = new ProductImage();
            $dataa->image = $path;
            $dataa->product_id = $product_id;
            $dataa->save();
        }
        foreach (request()->value as $key => $option) {
            if ($option) {
                $dataa = new ProductOption();
                $dataa->option_value_id = $option;
                $dataa->product_id = $product_id;
                $dataa->option_id = request()->option_id[$key];
                $dataa->save();
            }
        }
        return redirect('/webadmin/products')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم اضافة المنتج بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::where('id', $id)->first();
        return view('admin.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::with('ProductImage')->where('id', $id)->first();
        $categories = Category::where('parent_id', null)->get();
        $cities = City::get();
        return view('admin.products.edit', compact('product','categories','cities'));
    }
   public function ProductData($product_id , $category_id)
    {
        $parent_id  = DB::table('categories')->where('id','=',$category_id)->select('parent_id')->first();
        $sup_category  = DB::table('categories')->where('parent_id','=',$parent_id->parent_id)->get();
//        $category_option  = DB::table('options as t1')
//                            ->leftJoin('product_options as t2',function ($join) use ($product_id) {
//                                $join->on('t2.option_id', '=' , 't1.id') ;
//                                $join->where('t2.product_id','=',$product_id) ;
//                            })
//                            ->where('t1.category_id','=',$category_id)
//                            ->select('t1.id','t1.name_ar','t2.option_value_id')
//                            ->get();
//
                           $category_option  = Option::with('Values2')
                               ->leftJoin('product_options as t3',function ($join) use ($product_id) {
                                $join->on('t3.option_id', '=' , 'options.id') ;
                                $join->where('t3.product_id','=',$product_id) ;
                            })
                            ->where('options.category_id','=',$category_id)
                            ->select('options.id','options.name_ar','t3.option_value_id')
                            ->get();


//                           return $category_option ;
        $data = ['parent_id' =>$parent_id ,'sup_category' =>$sup_category,'category_option'=>$category_option] ;
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'phone' => 'required|string',
            'price' => 'required|string',
            'description' => 'sometimes|nullable|string',
            'type' => 'required',
            'city_id' => 'required',
            'images' => 'array',
            'images.*' => 'mimes:png,jpeg,jpg|max:8000',
            'options' => 'array',
        ]);

        $images = request()->file('images');
        $images_pathes = [];

        if (count($images ??[])){
            foreach ($images as $key => $image) {
                $imageName = Str::random(10) . '.' . $image->extension();
                $image->move(
                    base_path() . '/public/uploads/products/', $imageName
                );
                $images_pathes[$key] = '/uploads/products/' . $imageName;
            }
        }


        $data = [
            'user_id' => auth()->id() ?? 2,
            'title' => $request->title,
            'phone' => $request->phone,
            'price' => $request->price,
            'description' => $request->description,
            'category_id' => $request->type,
            'city_id' => $request->city_id,
        ];
        $product = Product::where('id', $id)->update($data);
        DB::table('product_options')->where('product_id', $id)->delete();
//        DB::table('product_images')->where('product_id', $request->product_id)->delete();
        foreach ($images_pathes as $key => $path) {
            $dataa = new ProductImage();
            $dataa->image = $path;
            $dataa->product_id = $id;
            $dataa->save();
        }


        ProductOption::where('product_id', $id)->delete();

        foreach (request()->value as $key => $value) {
            if ($value) {
                $dataa = new ProductOption();
                $dataa->option_value_id = $value;
                $dataa->product_id = $id;
                $dataa->option_id = request()->option_id[$key];
                $dataa->save();
            }
        }
        return redirect('/webadmin/products' )->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل المنتج بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Product::where('id', $id)->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف المنتج بنجاح']));
    }


}
