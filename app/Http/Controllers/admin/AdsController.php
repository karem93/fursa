<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdsController extends Controller
{
    public function index()
    {
        $sliders = Slider::orderBy('created_at', 'ASC')->get();
        return view('admin.sliders.index', compact('sliders'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);

        return view('admin.sliders.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $slider = Slider::find($id);
        $request->validate([
            'type' => 'required',
            'status' => 'nullable',
            'code' => 'nullable'
        ]);

        $slider->update([
            'type' => $request->type??1,
            'status' => $request->status??1,
            'code' => $request->code
        ]);
        if ($request->path) {
            $imageName = Str::random(10) . '.' . $request->file('path')->extension();
            $request->file('path')->move(
                base_path() . '/public/uploads/sliders/', $imageName
            );
            $slider->path = '/uploads/sliders/' . $imageName;
            $slider->save();
        }
        return redirect('/webadmin/ads')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل الاعلان بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slider::destroy($id);
        return redirect('/webadmin/sliders')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف البنر بنجاح']));

    }
}
