<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Option;
use App\Models\OptionValue;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoriesOptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $options = DB::table('options as t1')
            ->leftJoin('categories as t2', 't1.category_id', '=', 't2.id')
            ->select('t1.*', 't2.name_ar as category_ar', 't2.name_en as category_en')
            ->where(function ($q) use ($request) {
                if ($request->has('category_id') && $request->get('category_id') != '') {
                    $q->where('t1.category_id', $request->get('category_id'));
                }
            })
            ->orderBy('created_at', 'ASC')->paginate(10);
        $categories = Category::all();
        $category_id = $request->get('category_id') ?? '';
        return view('admin.categories-options.index', compact('options', 'categories', 'category_id'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $option = Option::find($id);
        $values = OptionValue::where('option_id', $id)->get();
        return view('admin.categories-options.edit', compact('option', 'values'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name_ar' => 'required',
            'name_en' => 'required',
        ]);
        $option = Option::find($id);
        $option->update($request->except('value_id', 'value_en', 'value', 'icon'));

        if ($request->hasFile('icon')) {
            $request->validate([
                'icon' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);


            $imageName = Str::random(10) . '.' . $request->file('icon')->extension();
            $request->file('icon')->move(
                base_path() . '/public/uploads/', $imageName
            );
            if ($option->icon) {
                if (\File::exists($option->icon)) {
                    unlink($option->icon);
                }
            }
            $option->icon = '/uploads/' . $imageName;
            $option->save();
        }

        foreach (request()->value as $key => $value) {
            if ($value != null) {
                if (isset(request()->value_id[$key])) {
                    $option_val = OptionValue::find(request()->value_id[$key]);
                    $option_val->update(
                        [
//                        'id' => request()->value_id[$key],
//                        'option_id' => $id,
                            'value' => $request->value[$key],
                            'value_en' => $request->value_en[$key],
                        ]
                    );
                } else {
                    OptionValue::create([
                        'option_id' => $id,
                        'value' => $request->value[$key],
                        'value_en' => $request->value_en[$key],
                    ]);
                }
            }
        }
        return redirect('/webadmin/categories-options')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل الخاصيه بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Option::destroy($id);
        return redirect('/webadmin/categories-options')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف الخاصيه بنجاح']));
    }
}
