<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Contact;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SettingsController extends Controller
{
    public function about_us()
    {
        $setting = Setting::first();
        $categories = Category::where('parent_id', null)->get();
        return view('website.about-us', compact('setting', 'categories'));
    }

    public function contact()
    {
        $setting = Setting::first();
        $categories = Category::where('parent_id', null)->get();
        return view('website.contact-us', compact('setting', 'categories'));
    }

    public function contact_us(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'category_id' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        if (request()->hasFile('image')) {
            $imageName = Str::random(10) . '.' . request()->File('image')->extension();
            request()->File('image')->move(
                base_path() . '/public/uploads/', $imageName
            );
            $image = '/uploads/' . $imageName;
        }
        $contact = new Contact;
        $contact->email = $request->email;
        $contact->description = $request->description;
        $contact->category_id = $request->category_id;
        $contact->image = $request->image ? $image : null;
        $contact->save();

        session()->flash('success', trans('site.send_success'));
        return redirect(url('/'));
    }

    public function adv()
    {
        $setting = Setting::first();
        $categories = Category::where('parent_id', null)->get();
        return view('website.adv', compact('setting', 'categories'));

    }

    public function privacy()
    {
        $categories = Category::where('parent_id', null)->get();
        $setting = Setting::first();
        return view('website.privacy', compact('setting', 'categories'));

    }

    public function terms()
    {
        $setting = Setting::first();
        $categories = Category::where('parent_id', null)->get();
        return view('website.terms', compact('setting', 'categories'));

    }

    public function packages()
    {
        $categories = Category::where('parent_id', null)->get();
        return view('website.packages', compact('categories'));
    }
}
