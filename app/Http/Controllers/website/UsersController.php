<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use App\Models\Fav;
use App\Models\Message;
use App\Models\Product;
use App\Notifications\NewMessageNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    public function profile()
    {
        $user = Auth::user();
        $countries=Country::all();

        if ($user->country_id){
            $city=City::where('country_id',$user->country_id)->get();
        }else{
            $city = [];
        }

        $products = Product::where('user_id', $user->id)->get();
        return view('website.user.profile', compact('user', 'products','countries','city'));

    }
    public function profileUpdate(Request $request)
    {

        $id = \auth()->user()->id ;
        $data = $this->validate(request(),[
            'username'=>'required',
            'email'=>'required|email|unique:users,email,'.$id,
            'phone'=>'required|string|unique:users,phone,'.$id,
            'password'=>'nullable|min:6|confirmed',
            'country_id'=>  'required',
            'city_id'=> 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        $updated_admin = User::findOrFail($id);
        if (request()->hasFile('photo')) {
            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $data['photo'] = '/uploads/profiles/' . $imageName;
        }
        $updated_admin->username = $data['username'];
        $updated_admin->email = $data['email'];
        $updated_admin->phone = $data['phone'];
        $updated_admin->country_id = $data['country_id'];
        $updated_admin->city_id = $data['city_id'];
        if ($request->password) {
            $updated_admin->password = Hash::make($data['password']);
        }
        if (request()->hasFile('photo')) {
            $updated_admin->photo = $data['photo'];
        }
        $updated_admin->save();
        session()->flash('success','تم تعديل بيانات ملفك الشخصي بنجاح');
        return redirect(url('profile'));
    }

    public function favourites()
    {
        $user = Auth::user();
        $ids = Fav::where('user_id', $user->id)->pluck('product_id')->toArray();
        $products = Product::whereIn('id', $ids)->get();
        return view('website.user.favourites', compact('products'));
    }

    public function chatDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $to_user = User::where('id', $request->user_id)->first();

        $data = Message::select(['id', 'message', 'file', 'receiver_id', 'sender_id', 'is_seen', 'created_at'])
            ->where(function ($query) use ($user, $to_user) {
                $query->where([['sender_id', $user->id], ['receiver_id', $to_user->id]]);
                $query->orWhere([['sender_id', $to_user->id], ['receiver_id', $user->id]]);
            })->orderBy('created_at', 'asc')->with(['sender' => function ($q) {
                $q->select(['id', 'name', 'tokens', 'photo'])->get();
            }]);
        $messages = $data->get();
        $messages->transform(function ($i) {
            $i->since = $i->created_at->diffForHumans();
            if ($i->file) {
                $i->file = \Helpers::base_url() . '/' . $i->file;
            }
            return $i;
        });

        //read messages
        $data->update(['is_seen' => 1]);
        return response()->json([
            'messages' => $messages,
        ], 200);
    }

    public function sendMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'receiver_id' => 'required',
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        $to_user = User::where('id', $request->receiver_id)->first();

        $message = Message::create([
            'sender_id' => $user->id,
            'receiver_id' => $to_user->id,
            'message' => $request->message,
        ]);


        //notification
        $title = 'فرصة';
        $content = 'رسالة جديدة';
        $fcm_message = [
            'message_id' => (int)$message->id,
            'sender_id' => (int)$user->id,
            "is_seen" => (int)$message->is_seen,
            "message" => $message->message ?: null,
            'since' => $message->created_at->diffForHumans(),
            'created_at' => $message->created_at,
            'type' => 'message',
            "sender" => [
                'id' => $user->id,
                'name' => $message->sender->name ?: "",
                'photo' => \Helpers::base_url() . $message->sender->photo ?: "",
                'tokens' => $message->sender->tokens,
            ]
        ];

        Notification::send($to_user, new NewMessageNotification($message));
        \Helpers::fcm_notification($to_user->device_token, $content, $title, $fcm_message);

        return response()->json([], 204);
    }

    public function chats()
    {
        $user = Auth::user();
        $received_messages = Message::select('*')->
        from(DB::raw("
    ( 
      SELECT *,`sender_id` as `theuser`
      FROM `messages` WHERE `receiver_id` = $user->id
      union
      SELECT *,`receiver_id` as `theuser`
      FROM `messages` WHERE `sender_id` = $user->id
      order by `created_at` desc
    ) as `sub`"))->groupBy('theuser')->
        orderBy('created_at', 'desc')->
        get();


        if ($received_messages->isNotEmpty()) {
            $recent_chat = Message::where([['sender_id', $received_messages[0]->sender_id], ['receiver_id', $received_messages[0]->receiver_id]])
                ->orWhere(function ($query) use ($user, $received_messages) {
                    return $query->where([['sender_id', $received_messages[0]->receiver_id], ['receiver_id', $received_messages[0]->sender_id]]);
                })->get();
        } else {
            $recent_chat = collect();
        }
        return view('website.user.chat', compact('received_messages', 'recent_chat', 'user'));

    }


}
