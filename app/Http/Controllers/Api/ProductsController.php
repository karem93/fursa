<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\OptionCollection;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductDetails;
use App\Http\Resources\UserProfileResource;
use App\Models\Category;
use App\Models\City;
use App\Models\Option;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class ProductsController extends Controller
{
    public function getCategories($id)
    {
        if ($id == 0) {
            $categories = Category::where('parent_id', null)->get();
        } else {
            $categories = Category::where('parent_id', $id)->get();
        }
        return response()->json(new CategoryCollection($categories), 200);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCategoryOptions($id)
    {
        $SubCategory = Option::where('category_id', $id)->get();
        return response()->json(new OptionCollection($SubCategory), 200);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCategoryOptionsSabryUpdate(Request $request)
    {
        $SubCategory = Option::whereIn('category_id', \request()->category)->get();
        return response()->json(new OptionCollection($SubCategory), 200);
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function AddProduct(Request $request)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        self::Validation($request);


        $data = [
            'user_id' => $user->id,
            'title' => $request->title,
            'phone' => $request->phone,
            'price' => $request->price ?: null,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'city_id' => $request->city_id,
        ];
        $product = Product::create($data);
        $product_id = $product->id;

        if ($request->hasFile('images')) {
            $images = request()->file('images');
            $images_pathes = [];

            foreach ($images as $key => $image) {
                $imageName = Str::random(10) . '.' . $image->extension();
                $image->move(
                    base_path() . '/public/uploads/products/', $imageName
                );
                $images_pathes[$key] = '/uploads/products/' . $imageName;
            }
            foreach ($images_pathes as $key => $path) {
                $dataa = new ProductImage();
                $dataa->image = $path;
                $dataa->product_id = $product_id;
                $dataa->save();
            }
        }
        $ad_options = json_decode($request->options, true);
        foreach ($ad_options as $option) {
            $dataa = new ProductOption();
            $dataa->product_id = $product_id;
            $dataa->option_id = $option['option_id'];
            $dataa->option_value_id = $option['option_value'];
            $dataa->save();
        }
        return response()->json('', 204);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function EditProduct(Request $request)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        self::Validation($request);
        $images = request()->file('images');
        $images_pathes = [];

        foreach ($images as $key => $image) {
            $imageName = Str::random(10) . '.' . $image->extension();
            $image->move(
                base_path() . '/public/uploads/products/', $imageName
            );
            $images_pathes[$key] = '/uploads/products/' . $imageName;
        }

        $data = [
            'user_id' => $user->id,
            'title' => $request->title,
            'phone' => $request->phone,
            'price' => $request->price ?: null,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'city_id' => $request->city_id,
        ];
        $product = Product::where('id', $request->product_id)->update($data);
        DB::table('product_options')->where('product_id', $request->product_id)->delete();
        DB::table('product_images')->where('product_id', $request->product_id)->delete();
        foreach ($images_pathes as $key => $path) {
            $dataa = new ProductImage();
            $dataa->image = $path;
            $dataa->product_id = $request->product_id;
            $dataa->save();
        }

        $options = json_decode($request->options, true);
        foreach ($options as $option) {
            $dataa = new ProductOption();
            $dataa->product_id = $request->product_id;
            $dataa->option_id = $option['option_id'];
            $dataa->option_value_id = $option['option_value'];
            $dataa->save();
        }
        return response()->json(null, 204);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function DeleteProduct($id)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        Product::where('id', $id)->delete();
        DB::table('product_options')->where('product_id', $id)->delete();
        DB::table('product_images')->where('product_id', $id)->delete();
        return response()->json(null, 204);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getProductDetails($id)
    {
        $product = Product::where('id', $id)->first();
        $product->update([
            'views' => $product->views++
        ]);
        return response()->json(new ProductDetails($product), 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function SearchProduct(Request $request)
    {
        if ($request->has('options')) {
            $first = true;
            $options = DB::table('product_options')
                ->where(function ($q) use ($request, $first) {
                    $ad_options = json_decode($request->options, true);
                    foreach ($ad_options as $option) {
                        if ($first) {
                            $q->where('option_id', '=', $option['option_id']);
                            $q->where('option_value_id', '=', $option['option_value']);
                            $first = false;
                        } else {
                            $q->orWhere('option_id', '=', $option['option_id']);
                            $q->where('option_value_id', '=', $option['option_value']);
                        }
                    }
                })
                ->select(['product_id'])->distinct()->get();
            $request->activity_sectors_id = $options->pluck('product_id');
        }

        $products = Product::select('products.id', 'products.title', 'products.price',
            DB::raw('(select image from product_images where product_id  =   products.id   order by id asc limit 1) as image'))
            ->where(function ($q) use ($request) {
                if ($request->has('category_id') && $request->get('category_id') != '') {
                    $q->where('products.category_id', '=', $request->get('category_id'));
                }
                if ($request->has('city_id') && $request->get('city_id') != '') {
                    $q->where('products.city_id', '=', $request->get('city_id'));
                }
                if ($request->has('title') && $request->get('title') != '') {
                    $q->where('products.title', 'like', "%{$request->get('title')}%");
                }
                if ($request->has('price_from') && $request->get('price_from') != '') {
                    $q->where('products.price', '>=', $request->get('price_from'));
                }
                if ($request->has('price_to') && $request->get('price_to') != '') {
                    $q->where('products.price', '<=', $request->get('price_to'));
                }
                if ($request->activity_sectors_id) {
                    $q->whereIn('products.id', $request->activity_sectors_id);
                }
            })
            ->paginate($request->get('paginate'));
        return response()->json($products, 200);
    }

    public function getMyProducts()
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $products = Product::where('user_id', $user->id)->whereHas('category')->select(['id', 'title', 'price', 'city_id', 'category_id'])->get();
        $products->transform(function ($i) {
            $image = ProductImage::where('product_id', $i->id)->first();


            $i->image = $image ? \Helpers::base_url() . '/' . $image->image : '';
            return $i;

        });
        return response()->json(['products' => new ProductCollection($products)], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCatProduct($id)
    {
        if (\Helpers::getCurrentCity() == 0) {
            $country_id = \Helpers::getCurrentCountry();
            $cities = City::where('country_id', $country_id)->pluck('id');
            $products = Product::where('category_id', $id)->whereIn('city_id', $cities)->get();
        } else {
            $city_id = \Helpers::getCurrentCity();
            $products = Product::where('category_id', $id)->where('city_id', $city_id)->get();
        }
        return response()->json(new ProductCollection($products), 200);
    }

    /**
     * @param $product_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductOwner($product_id)
    {
        $product = Product::where('id', $product_id)->first();
        return response()->json(new UserProfileResource($product->user), 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function SearchProductByName(Request $request)
    {
        $searchProducts = Product::where('title', 'LIKE', '%' . $request->name . '%')->paginate(10);
        return response()->json(ProductResource::collection($searchProducts)->response()->getData(true), 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    private function Validation(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'phone' => 'required|string',
            'price' => 'required|string',
            'description' => 'sometimes|nullable|string',
            'category_id' => 'required',
            'city_id' => 'required',
            'images' => 'array',
            'images.*' => 'mimes:png,jpeg,jpg|max:8000',
            'options' => 'array',
        ], [], [
            'title' => trans('app.title'),
            'phone' => trans('app.phone'),
            'price' => trans('app.price'),
            'description' => trans('app.description'),
            'category_id' => trans('app.category'),
            'city_id' => trans('app.city'),
            'images' => trans('app.images'),
            'options' => trans('app.options'),
        ]);

        if ($validation->fails()) {
            return response()->json($validation->errors(), 400);
        }
    }


}
