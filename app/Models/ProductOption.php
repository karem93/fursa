<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    protected $table = 'product_options';
    protected $guarded = [];

    public $timestamps = true;
    public function option()
    {
        return $this->belongsTo(Option::class, 'option_id');
    }
    public function optionValue()
    {
        return $this->belongsTo(OptionValue::class, 'option_value_id');
    }

}
